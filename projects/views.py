from django.shortcuts import render, get_object_or_404, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import ProjectForm


# Create your views here.
@login_required
def project_view(request):
    all_projects = Project.objects.filter(owner=request.user)
    context = {"all_projects": all_projects}
    return render(request, "projects/list_projects.html", context)


@login_required
def show_project(request, id):
    this_project = get_object_or_404(Project, id=id)
    context = {"this_project": this_project}
    return render(request, "projects/show_project.html", context)


@login_required
def create_project(request):
    if request.method != "POST":
        form = ProjectForm
    else:
        form = ProjectForm(request.POST)
        if form.is_valid():
            new_form = form.save(False)
            new_form.owner = request.user
            new_form.save()
            return redirect("list_projects")

    context = {"form": form}
    return render(request, "projects/create_project.html", context)
